# Conky-Config

A config for conky, a personalized fork of a fork as well as a few python scripts that make it work

Requirements, conky

$ sudo apt install conky

$ sudo cp /path/to/conky-config/conky.conf /etc/conky/

I added the Pyhon scripts that I use to pull Pihole data into my conky.

This means you'll have to edit all the conky.conf refrences to where the Scripts are held.

You'll also have to edit the edit the .py files and replace my pi's IP with your own.
